GADS stands for Geo Android Debug System.
Written in portable Java, it's a framework born with the target to make the debugging
of Geo-based Android applications easier and faster.