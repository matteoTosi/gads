REM /bin/bash

REM classpath 
set GADS_HOME="."
REM main file of jpintp 
set MAIN=com.gads.view.Main 


REM BASE CLASSES
set CP="%GADS_HOME%\bin\"
set CP=%CP%":%GADS_HOME%\lib\antlr-4.0-complete.jar"
set CP=%CP%":%GADS_HOME%\lib\jta26.jar"
set CP=%CP%":%GADS_HOME%\lib\sqlite-jdbc-3.7.15-M1.jar"


java -cp "%CP%" "%MAIN%" %*